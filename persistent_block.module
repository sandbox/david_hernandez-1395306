<?php

/**
 * @file
 * You can configure a block to be always visible in the screen.
 * 
 * TODO: Validation in the settings form.
 */

/**
 * Implements hook_perm().
 */
function persistent_block_perm() {
  return array('administer persistent block', 'view persistent block');
}

/**
 * Implements hook_menu().
 */
function persistent_block_menu() {
  $menu['admin/settings/persistent-block'] = array(
    'title' => t('Persistent block settings'),
    'description' => t('Configure a block to be always visible in the screen'),
    'access arguments' => array('administer persistent block'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('persistent_block_admin_form'),
  );
  return $menu;
}

/**
 * Page callback for admin/settings/persistent-block
 */
function persistent_block_admin_form(&$form_state) {
  $form['add-block']['#value'] = l('Add a new block', 'admin/build/block/add', array('query' => array('destination' => 'admin/settings/persistent-block')));

  // Gets the list of blocks.
  $blocks = _persistent_block_get_available_blocks();
  // Select a block to be used as the persistent block.
  $form['persistent_block'] = array(
    '#title' => t('Block'),
    '#description' => t('Select the block to use as persistent block'),
    '#type' => t('select'),
    '#options' => $blocks,
    '#default_value' => variable_get('persistent_block', ''),
  );

  // Fieldset for all the style settings.
  $form['style'] = array(
    '#title' => t('Style of the block'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Get all the styles available.
  $styles = persistent_block_get_styles_list();
  // Select form for the styles of the persistent block.
  $form['style']['persistent_block_style'] = array(
    '#title' => t('Style'),
    '#description' => t('Select a style for the persistent block. Depending of the style, the position of the block may vary.'),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => $styles,
    '#default_value' => variable_get('persistent_block_style', ''),
  );
  // Checkbox to select if the block can be collapsible.
  $form['style']['persistent_block_collapsible'] = array(
    '#title' => t('Will the block be collapsible?'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('persistent_block_collapsible', FALSE),
  );
  // Checkbox to select if the block will start collapsed.
  $form['style']['persistent_block_collapsed'] = array(
    '#title' => t('Will the block start collapsed?'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('persistent_block_collapsed', FALSE),
  );

  // Get the list of roles.
  $roles = user_roles();
  // Fieldset for all the settings belonging to the role visibility.
  $form['role_visibility'] = array(
    '#title' => t('Role specific visibility settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Restrict the persistent block for user with selected roles.
  $form['role_visibility']['persistent_block_roles'] = array(
    '#title' => t('Show block for specific roles'),
    '#description' => t('Show this block only for the selected role(s). If you select no roles, the block will be visible to all users.'),
    '#type' => 'checkboxes',
    '#required' => TRUE,
    '#options' => $roles,
    '#default_value' => variable_get('persistent_block_roles', array()),
  );

  // Fieldset for all the settings belonging to the page visibility.
  $form['page_visibility'] = array(
    '#title' => t('Page specific visibility settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Select for the different options of showing the block in the different pages.
  $form['page_visibility']['persistent_block_page_settings'] = array(
    '#title' => t('Show block on specific pages'),
    '#type' => 'radios',
    '#required' => TRUE,
    '#options' => array(
      0 => t('Show on every page except the listed pages.'),
      1 => t('Show on only the listed pages.'),
      2 => t('Show if the following PHP code returns TRUE (PHP-mode, experts only).'),
    ),
    '#default_value' => variable_get('persistent_block_page_settings', ''),
  );
  // List of pages or PHP Code.
  $description = 'Enter one page per line as Drupal paths. ';
  $description .= 'The \'*\' character is a wildcard. ';
  $description .= 'Example paths are blog for the blog page and blog/* for every personal blog. ';
  $description .= '<front> is the front page. ';
  $description .= 'If the PHP-mode is chosen, enter PHP code between <?php ?>. ';
  $description .= 'Note that executing incorrect PHP-code can break your Drupal site.';
  $form['page_visibility']['persistent_block_pages'] = array(
    '#title' => t('Pages'),
    '#description' => t($description),
    '#type' => 'textarea',
    '#required' => FALSE,
    '#default_value' => variable_get('persistent_block_pages', ''),
  );

  // Submit button added by the system settings form.
  return system_settings_form($form);
}

/**
 * Get the list of available blocks ready to be used as options in a form.
 */
function _persistent_block_get_available_blocks() {
  $blocks = _block_rehash();
  $list = array();
  foreach ($blocks as $block) {
    $list[$block['bid']] = $block['info'];
  }
  return $list;
}

/*
 * Get the list of persistent block styles, ready to be used as options in a form.
 */
function persistent_block_get_styles_list() {
  // Get the full list of styles.
  $reset = TRUE;
  $styles = persistent_block_get_styles($reset);

  $list = array();
  // Process the styles to get a list prepared for use as options of a form.
  foreach ($styles as $id => $style) {
    $list[$id] = $style['name'];
  }
  return $list;
}

/**
 * Gets the list of styles for the persistent block.
 */
function persistent_block_get_styles($reset = FALSE) {
  static $styles;
  if (empty($styles) || $reset == TRUE) {
    $styles = array();
    $hook = 'persistent_block_styles';
    foreach (module_implements($hook) as $module) {
      $function = $module . '_' . $hook;
      $function($styles);
    }
  }
  return $styles;
}

/**
 * Implements hook_persistent_block_styles().
 */
function persistent_block_persistent_block_styles(&$styles) {
  $styles['top_right'] = array(
    'name' => t('Top right corner'),
    'attributes' => array(
      'id' => 'persistent-block-top-right',
      'class' => array(
        'persistent-block',
        'top-right',
      ),
    ),
    'style' => '',
  );
}

/**
 * Implements hook_footer().
 */
function persistent_block_footer($main) {
  global $user;
  // Control flags.
  $user_has_role = FALSE;
  $add_block = FALSE;
  // Value from administration settings form.
  $enabled_roles = variable_get('persistent_block_roles', array());
  // Check if the user has the role.
  foreach ($user->roles as $role_id => $role) {
    if (in_array($role_id, $enabled_roles)) {
      $user_has_role = TRUE;
    }
  }
  // If the user has the role, proceed.
  if ($user_has_role) {
    $page_settings = variable_get('persistent_block_page_settings', '');
    // Check the different page visibility settings.
    switch ($page_settings) {
      // Show on every page except the listed pages.
      case 0:
        // Get the exceptions
        $exceptions = variable_get('persistent_block_pages', '');
        // As by default all the pages can go, set the flag to TRUE.
        $add_block = TRUE;
        // If there are no exceptions, add the block.
        if (empty($exceptions)) {
          break;
        }
        // Get the current path.
        $current_path = $_GET['q'];
        $exceptions = explode("\n", $exceptions);
        foreach ($exceptions as $path) {
          $path = explode('/', $path);
          if ($path == $current_path) {
            $add_block = FALSE;
            break;
          }
          $current_path = explode('/', $current_path);
          $is_same_path = TRUE;
          for ($i = 0; $i < count($path); $i++) {
            if ($path[$i] != $current_path[$i]) {
              $is_same_path = FALSE;
            }
          }
        }
        // Do whatever
        break;
      // Show on only the listed pages
      case 1:
        // Do whatever
        break;
      // Show if the following PHP code returns TRUE.
      case 2:
        $php = variable_get('persistent_block_pages', '');
        if (empty($php)) {
          drupal_set_message('Please, configure properly the persistent block settings.', 'warning');
          break;
        }
        if (eval($php)) {
          $add_block = TRUE;
        }
        // Do whatever
        break;
    }
  }
  if ($add_block) {
    $block;
    return theme('persistent_block', $block);
  }
}

/**
 * Implements hook_theme().
 */
function persistent_block_theme() {
  return array(
    'persistent_block' => array(
      'arguments' => array(
        'block' => NULL,
      ),
    ),
  );
}

/**
 * Theme function for the persistent_block.
 */
function theme_persistent_block($block) {
  return '';
}